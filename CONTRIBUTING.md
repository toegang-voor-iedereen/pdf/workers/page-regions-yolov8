# Contributing Guide

## Development Setup

1. Clone the project:

```bash
git clone https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo.git
cd project
```

2. Install Poetry:

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

3. Install dependencies:

```bash
poetry install
```

4. Install shell plugin:

```bash
poetry self add poetry-plugin-shell
```

5. Activate virtual environment:

```bash
poetry shell
```

## Code Standards

- PEP 8 style guidelines
- Type hints required
- Docstrings for public functions
- Test coverage may not decrease
- Black for formatting
- Ruff for linting

## Testing

To run the unit tests, use the following command:

```bash
pytest --cov=./ ./
```

### Testing guidelines

TBD

## Pre-commit Hooks

```bash
pre-commit install
pre-commit run --all-files
```

## Merge Request Process

1. Create feature branch:

```bash
git checkout -b feature/name
```

2. Commit using [conventional commits](https://www.conventionalcommits.org/):

```bash
git commit -m "feat: description"
```

3. Push and create MR via GitLab

## Requirements

- Python >=3.12
