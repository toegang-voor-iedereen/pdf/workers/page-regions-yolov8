from statistics import mean
from kimiworker import ContentDefinition


def calculate_mean_confidence(content_result: list[ContentDefinition]) -> float:
    """
    Calculate the mean confidence for the content regions found

    Args:
        - content_result (list[ContentDefinition]): List of content definitions

    Returns:
        - float: the mean confidence of all result items
    """
    return (
        mean([content.get("confidence") for content in content_result])
        if content_result
        else 0
    )
