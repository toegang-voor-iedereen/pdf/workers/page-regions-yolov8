# Changelog

## [1.17.4](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.17.3...1.17.4) (2025-03-06)


### Bug Fixes

* **deps:** update dependency kimiworker to v4.5.0 ([d4bd0aa](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/d4bd0aa38b14062e7874b14fdf4dde4ade496e68))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.3 ([80856d6](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/80856d6fdfe4a16b2b79bba9b915f184a277da05))

## [1.17.3](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.17.2...1.17.3) (2025-03-06)


### Bug Fixes

* **deps:** update dependency pytest to v8.3.5 ([ddfbbef](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/ddfbbef92cf8e364ea5d6189de228413febe3eb2))
* **deps:** update dependency ultralytics to v8.3.67 ([da412a9](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/da412a9f227f3bce5e8c2375cf169e24ce09d8ef))
* **deps:** update dependency ultralytics to v8.3.69 ([a76bf3d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/a76bf3d88b098fb3c8eb84fd9fd1abb7efca045b))
* **deps:** update dependency ultralytics to v8.3.70 ([c81dfed](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/c81dfed8c64758009aafac53215252213967313c))
* **deps:** update dependency ultralytics to v8.3.72 ([6b66e03](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/6b66e0349afd942dc595763b9bb046cf449e55ae))
* **deps:** update dependency ultralytics to v8.3.73 ([1107dec](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/1107deceda08169786397ee22565e60102dc18d6))
* **deps:** update dependency ultralytics to v8.3.78 ([d0e9cee](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/d0e9ceed6e30550df0794082beb4055b04ef27e2))
* **deps:** update dependency ultralytics to v8.3.80 ([8d64a9f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/8d64a9fab9f6acae9871fe2f925d3eb2f0a25905))
* **deps:** update dependency ultralytics to v8.3.83 ([22460de](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/22460de73e44710f2305a2632fe16bb5279ed86a))
* **deps:** update dependency ultralytics to v8.3.84 ([ce02538](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/ce02538a2c746c5f88c888f59a61ed876c5d6008))

## [1.17.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.17.1...1.17.2) (2025-01-20)


### Bug Fixes

* **deps:** update dependency kimiworker to v4.4.3 ([8a60aae](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/8a60aae7090773bb7cc7f7859f4c2ec77b6d355d))
* **deps:** update dependency ultralytics to v8.3.63 ([09000be](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/09000be0025fa4bcb66f829532f6ac9a2bbce586))
* **deps:** update dependency ultralytics to v8.3.64 ([1c51eff](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/1c51effa84cdd1aeb86af77098d5ce7760a848aa))

## [1.17.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.17.0...1.17.1) (2025-01-16)


### Bug Fixes

* **deps:** update dependency ultralytics to v8.3.62 ([851195e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/851195efced9e9e1b6100e339719118fe595b0df))

# [1.17.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.16.0...1.17.0) (2025-01-16)


### Features

* add contribution guide ([78b4a54](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/78b4a5423c2e20529c0f80144f16b0e6b63c4898))
* add docker-compose.yaml for easy setup ([b056367](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/b0563673a831af4ff4d5acabb7ded920a87e97a1))
* add integration notebook ([fbcc78e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/fbcc78e5ab12f6cbbfbb3f2a4a8eff046bd77638))

# [1.16.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.15.0...1.16.0) (2024-12-17)


### Features

* **deps:** kimiworker 4.4.0, removed minio dependency ([979d0f1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/979d0f1d3172252acce481cf389992f3072f3e8e))

# [1.15.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/compare/1.14.1...1.15.0) (2024-12-05)


### Bug Fixes

* remove CPU default ([b0e3c24](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/b0e3c24a3bf893553e8bebcebd33755ec0a90d2b))


### Features

* use YOLOv11 model ([991f609](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/commit/991f6094ce0478ba8c1fba0e6d5a08f5da663d13))

## [1.14.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.14.0...1.14.1) (2024-12-03)


### Bug Fixes

* handle mean confidence of empty content_result ([660ccda](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/660ccdacbf0ad3c38ba5d3b42ef1ece4e856918d))

# [1.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.13.0...1.14.0) (2024-11-19)


### Features

* force release ([cff153d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/cff153d5a0166fc3f4414e5de85f50e37eac832e))

# [1.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.12.0...1.13.0) (2024-11-12)


### Bug Fixes

* removed reference to nldocspec ([69ad525](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/69ad525952e2ab4375caeb91a33430f63adc166f))


### Features

* revert back to ContentDefinition ([744b76a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/744b76add5c0855ad17ec70b5c04129b3d0752a8))

# [1.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.11.0...1.12.0) (2024-11-06)


### Bug Fixes

* use kimiworker alpha to use ContentDefinition ([4dfd15c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/4dfd15c32a75ed2a0187832b8f220525ab41c58d))


### Features

* kimiworker 3.5.0 ([ec9cabb](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/ec9cabb3ec1ec79b94f60331657c70683e687323))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.10.0...1.11.0) (2024-10-22)


### Features

* use kimiworker 2.15.0 with nldocspec 4.1.1 ([b7b0370](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/b7b0370ff74aeca2222640b0862a523626051e2a))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.9.0...1.10.0) (2024-10-04)


### Features

* using kimiworker 2.8.0 to use nldocspec ([1effe5a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/1effe5aedefdee53c72f394d3b018a7339f52372))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.8.0...1.9.0) (2024-09-12)


### Features

* added volume mount for /tmp ([979778b](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/979778b5798e241fae9a410be9afabc86cbf09c8))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.7.0...1.8.0) (2024-09-04)


### Features

* added securityContext to container and initContainers ([f7968ac](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/f7968ac39831ff46b0714371dd2fa6192990a053))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.6.0...1.7.0) (2024-08-28)


### Features

* use new kimiworker to connect to station 4.0.0 and up ([e086045](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/e0860451f4aa3c818b9df920c1b12c485f5504a0))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.5.0...1.6.0) (2024-07-01)


### Features

* support initContainers in helm values ([41ad92f](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/41ad92f60225b763eda31828755b846be9bb29fe))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.4.0...1.5.0) (2024-06-05)


### Features

* support initContainers in helm values ([c6d26df](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/c6d26df0e394be73b7fb51a4fd83973e8963f6ff))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.3.1...1.4.0) (2024-06-04)


### Bug Fixes

* confidence times 100 ([4bd9805](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/4bd9805527da081528cecdab594232df49fba9e5))
* incorrect multiplication in unit test ([3a10a25](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/3a10a25c55ad8e46e864ad3871a7952a7b3f776a))


### Features

* correct worker name in chart ([886527d](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/886527ddd9dc7d1e96410533c09dcbd4698527a4))

## [1.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.3.0...1.3.1) (2024-05-17)


### Bug Fixes

* use proper version of dill ([0e80736](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/0e807364cff9d3d2911fc74ce3b23374c7a619d6))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.2.0...1.3.0) (2024-05-16)


### Features

* use cpu torch ([412757e](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/412757e6c3f4e04d662d8c574e18cc58f0f57218))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.1.0...1.2.0) (2024-05-16)


### Features

* use cpu versions of torch ([fb5656c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/fb5656cbe45fff3f02e731baacf55ef78a206c33))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.2...1.1.0) (2024-05-16)


### Features

* rename worker ([1cedc88](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/1cedc88f21c535b3f7f027e13bf801c1db84aea6))

## [1.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.1...1.0.2) (2024-05-16)


### Bug Fixes

* add gcc compiler to image ([f4498a2](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/f4498a24c322ef1405937f897ed07bb4375ca0a6))

## [1.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/compare/1.0.0...1.0.1) (2024-05-16)


### Bug Fixes

* use python 3.12 slim ([fae93c8](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/fae93c8dd5ae998ffbf84c731ad824d7dc3edbbe))
* use pytorch image ([1b8c272](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/1b8c27229cd9b5a12f91be0c49dba787f9c014ec))

# 1.0.0 (2024-05-16)


### Bug Fixes

* add torch and torchvision ([127ba8a](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/127ba8a6f9d7da52a6fa2133111f7339dcdf45f6))


### Features

* add model ([45c5fed](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/45c5fedd7cd9635dc410795a6c77ebafa8adaaf7))
* yolov8 worker ([e49a89c](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolov8/commit/e49a89c00716e4bf98f4821c0ceee9bb0fcedef2))
