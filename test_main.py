from unittest.mock import Mock, patch
from kimiworker import KimiLogger, WorkerJob, RemoteFileStorageInterface
from main import get_job_handler

# Mocks
mock_log = Mock(spec=KimiLogger)
mock_job = Mock(spec=WorkerJob)
mock_remote_file_storage = Mock(spec=RemoteFileStorageInterface)
mock_publish = Mock()

mock_detections = [
    {
        "classification": "application/x-nldoc.region",
        "bbox": {"left": 0.1, "top": 0.2, "right": 0.3, "bottom": 0.4},
        "confidence": 95.0,
        "attributes": {},
        "labels": [{"name": "section_header", "confidence": 95.0}],
        "children": [],
    }
]


@patch("main.detect", return_value=mock_detections)
def test_job_handler(mock_detect):
    job_handler = get_job_handler()

    job_handler(
        log=mock_log,
        job=mock_job,
        job_id="test_job_id",
        local_file_path="test_local_file_path",
        remote_file_storage=mock_remote_file_storage,
        publish=mock_publish,
    )

    mock_detect.assert_called_once_with("test_local_file_path", mock_log)
    mock_log.info.assert_called_once_with("Job done")
    mock_publish.assert_called_once_with(
        "contentResult",
        mock_detections,
        success=True,
        confidence=95,
    )
