import pytest
from utils import calculate_mean_confidence
from kimiworker import ContentDefinition
from typing import List


def test_calculate_mean_confidence_empty_list():
    """
    Test that an empty list returns 0 as the mean confidence
    """
    assert calculate_mean_confidence([]) == 0


def test_calculate_mean_confidence_single_item():
    """
    Test mean confidence calculation with a single content item
    """
    content: List[ContentDefinition] = [{"confidence": 0.95}]  # type: ignore  # noqa: F821
    assert calculate_mean_confidence(content) == 0.95


def test_calculate_mean_confidence_multiple_items():
    """
    Test mean confidence calculation with multiple content items
    """
    content: List[ContentDefinition] = [
        {"confidence": 0.8},
        {"confidence": 0.9},
        {"confidence": 1.0},
    ]  # type: ignore
    assert calculate_mean_confidence(content) == 0.9


def test_calculate_mean_confidence_missing_confidence():
    """
    Test that a TypeError is raised when confidence key is missing
    """
    content: List[ContentDefinition] = [{"confidence": 0.8}, {}]  # type: ignore
    with pytest.raises(TypeError):
        calculate_mean_confidence(content)
