from unittest.mock import Mock, patch
import pytest
from PIL import Image
import numpy as np
import torch

from detect import detect


@patch("detect.docseg_model")
def test_detect_returns_content_definitions(mock_model, tmp_path):
    mock_model.names = {0: "Section-header", 1: "Text"}

    box = Mock()
    box.xyxyn = torch.tensor([[0.1, 0.2, 0.3, 0.4]])
    box.conf = torch.tensor([0.95])
    box.cls = torch.tensor([0])

    result = Mock()
    result.boxes.cpu.return_value = [box]
    mock_model.return_value = [result]

    test_img = Image.fromarray(np.zeros((100, 100, 3), dtype=np.uint8))
    img_path = tmp_path / "test.jpg"
    test_img.save(img_path)

    detections = detect(str(img_path))

    assert len(detections) == 1
    det = detections[0]
    bbox = det["bbox"]

    # Need to use approximate equality for tensor values since
    # floating point precision can vary slightly
    assert abs(bbox["left"] - 0.1) < 1e-6
    assert abs(bbox["top"] - 0.2) < 1e-6
    assert abs(bbox["right"] - 0.3) < 1e-6
    assert abs(bbox["bottom"] - 0.4) < 1e-6
    assert abs(det["confidence"] - 0.95) < 1e-6
    assert (det["labels"] or [])[0]["name"] == "section_header"


def test_detect_invalid_path():
    with pytest.raises(RuntimeError):
        detect("nonexistent.jpg")


def test_detect_invalid_image(tmp_path):
    # Create invalid image file
    invalid_path = tmp_path / "invalid.jpg"
    invalid_path.write_text("not an image")

    with pytest.raises(RuntimeError):
        detect(str(invalid_path))
