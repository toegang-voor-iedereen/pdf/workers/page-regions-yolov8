import json
import threading
import time
from pathlib import Path
from typing import Optional

import pika
import pika.exceptions
from minio import Minio
from pika.adapters.blocking_connection import BlockingChannel

test_file = Path("./integration_test/test.jpg")


def wait_for_exchange(
    connection: pika.BlockingConnection,
    exchange_name: str,
    max_attempts: int = 30,
    delay: float = 2,
) -> BlockingChannel:
    """Wait for RabbitMQ exchange to become available."""
    last_exception = None
    for _ in range(max_attempts):
        try:
            channel = connection.channel()
            channel.exchange_declare(exchange=exchange_name, passive=True)
            return channel
        except (
            pika.exceptions.ChannelClosedByBroker,
            pika.exceptions.ChannelWrongStateError,
        ) as e:
            last_exception = e
            time.sleep(delay)
    raise TimeoutError(
        f"Exchange {exchange_name} not available after {max_attempts} attempts"
    ) from last_exception


def setup_minio(test_file: Path, host="minio"):
    """Initialize MinIO client and upload test file."""
    client = Minio(
        endpoint=f"{host}:9000",
        access_key="minioadmin",
        secret_key="minioadmin",
        secure=False,
    )
    client.fput_object(
        bucket_name="files",
        object_name=test_file.name,
        file_path=str(test_file),
        content_type="image/jpeg",
    )


def process_message(
    message: dict, current_job_id: Optional[str]
) -> tuple[Optional[str], Optional[dict]]:
    """Process incoming RabbitMQ message."""
    if not current_job_id:
        job_id = message.get("jobId")
        print(f"Got job ID: {job_id}")
        return job_id, None
    elif message.get("jobId") == current_job_id:
        print(f"Got result for job {current_job_id}")
        return current_job_id, message
    print(f"Skipping non-matching job ID: {message.get('jobId')}")
    return current_job_id, None


def execute_worker(host: str | None) -> dict:
    # Setup
    setup_minio(test_file, host or "minio")

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=host or "rabbitmq",
            port=5672,
            credentials=pika.PlainCredentials("guest", "guest"),
        )
    )

    try:
        # Setup RabbitMQ channel and queue
        channel = wait_for_exchange(connection, "page-regions-job-results")
        result_queue = channel.queue_declare(queue="", exclusive=True, durable=True)
        queue_name = result_queue.method.queue
        channel.queue_bind(exchange="page-regions-job-results", queue=queue_name)

        # Prepare message handler
        job_id = None
        result_message = None

        def callback(ch, method, properties, body):
            nonlocal job_id, result_message
            message = json.loads(body)
            job_id, result = process_message(message, job_id)
            if result:
                result_message = result
                ch.stop_consuming()

        channel.basic_consume(
            queue=queue_name, on_message_callback=callback, auto_ack=True
        )

        # Send job
        worker_job = {
            "recordId": "test123",
            "bucketName": "files",
            "filename": test_file.name,
            "attributes": {},
        }

        channel.basic_publish(
            exchange="page-regions-jobs",
            routing_key="job.x",
            body=json.dumps(worker_job),
            properties=pika.BasicProperties(
                content_type="application/json", headers={"x-trace-id": "test"}
            ),
        )

        print("Job published, waiting for response...")

        # Set timeout and start consuming
        timeout = 30
        timer = threading.Timer(timeout, channel.stop_consuming)
        timer.start()

        try:
            channel.start_consuming()
        finally:
            timer.cancel()

        # Handle results
        if not job_id:
            raise TimeoutError(f"No job acknowledgment within {timeout}s")
        if not result_message:
            raise TimeoutError(f"No result for job {job_id} within {timeout}s")
        if not result_message.get("success"):
            raise RuntimeError(f"Job {job_id} failed: {result_message}")

        print(f"Result received: {result_message}")
        return result_message

    finally:
        # Cleanup
        channel.queue_delete(queue_name)
        channel.close()
        connection.close()


if __name__ == "__main__":
    execute_worker(None)
