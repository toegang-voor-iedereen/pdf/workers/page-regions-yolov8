# Page Regions YOLO

[![pipeline status](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/badges/main/pipeline.svg)](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/-/commits/main)
[![coverage report](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/badges/main/coverage.svg?job=test)](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/-/commits/main)
[![Latest Release](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/-/badges/release.svg)](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/-/releases)

A Python worker that detects page regions using [YOLO11](https://yolo11.com/), part of the [YOLO](https://www.ultralytics.com/yolo) (You Only Look Once) series of object detection models. This worker processes pages to identify and classify different regions within documents.

## Features

- Fast and accurate region detection using YOLO11
- Integration with RabbitMQ for message queuing
- MinIO support for object storage
- Docker and Kubernetes deployment ready

## Getting Started

### Local Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo.git
   cd page-regions-yolo
   ```

2. Install Poetry (dependency management):

   ```bash
   curl -sSL https://install.python-poetry.org | python3 -
   ```

3. Install dependencies:

   ```bash
   poetry install
   ```

4. Install shell plugin:

   ```bash
   poetry self add poetry-plugin-shell
   ```

5. Activate virtual environment:
   ```bash
   poetry shell
   ```

### Running the Worker

#### Using Docker Compose (Recommended)

1. Ensure Docker and Docker Compose are installed ([Installation Guide](https://docs.docker.com/compose/install/))

2. Start the services:

   ```bash
   docker compose up
   ```

3. Use the included Jupyter notebook `integration.ipynb` to perform an end to end interaction using Minio and RabbitMQ

4. Access the management interfaces:
   - RabbitMQ Console: `http://localhost:15672/`
   - MinIO Console: `http://localhost:9001/`

5. Once the `docker compose up` command has been exited using Ctrl+C, all containers relative to this instance will have shut down automatically.

#### Running Directly

To run the worker without Docker:

```bash
python3 main.py
```

For configuration options, refer to the [Python base worker config](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/-/blob/develop/kimiworker/config.py).

### Testing and Development

Use the included Jupyter notebook `inference.ipynb` to visualize inference results on sample pages:

1. Ensure all dependencies are installed (follow installation steps above)
2. Launch Jupyter notebook
3. Open `inference.ipynb` and run the cells to process sample pages

## Deployment

### Docker Images

Pre-built Docker images are available in our [container registry](https://gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo/container_registry).

Run the latest version:

```bash
docker run --rm registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo:latest
```

### Kubernetes Deployment

We provide a Helm chart for Kubernetes deployment:

```yaml
name: page-regions-yolo
version: <version>
repository: https://gitlab.com/api/v4/projects/57939128/packages/helm/stable
```

For detailed configuration options, see the [`helm/values.yaml`](./helm/values.yaml) file.

## Documentation

- [Contributing Guide](CONTRIBUTING.md)
- [License](LICENSE.md)
- [Base Worker Configuration](https://gitlab.com/toegang-voor-iedereen/pdf/workers/base/python3/-/blob/develop/kimiworker/config.py)

## License

This project is licensed under the European Union Public License 1.2 - see [LICENSE](LICENSE.md) for details.

## Acknowledgements

- [Ultralytics](https://www.ultralytics.com/) for their state-of-the-art [YOLO](https://www.ultralytics.com/yolo) object detection models
