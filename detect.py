from typing import Dict, List

import torch
from kimiworker import ContentDefinition, KimiLogger
from ultralytics import YOLO

DEVICE = (
    "cuda"
    if torch.cuda.is_available()
    else "mps" if torch.backends.mps.is_available() else "cpu"
)

docseg_model = YOLO("model/yolov11l-doclaynet.pt")

LABELS_MAPPING: Dict[str, str] = {
    "Caption": "caption",
    "Footnote": "footnote",
    "Formula": "formula",
    "List-item": "list_item",
    "Page-footer": "page_footer",
    "Page-header": "page_header",
    "Picture": "picture",
    "Section-header": "section_header",
    "Table": "table",
    "Text": "text",
    "Title": "title",
}

REVERSE_LABELS = {orig: converted for orig, converted in LABELS_MAPPING.items()}


def detect(path: str, logger: KimiLogger = KimiLogger()) -> List[ContentDefinition]:
    """
    Detect and classify document components in an image using YOLOv8.

    Args:
        path (str): Path to input image file
        logger (KimiLogger, optional): Logger instance for error handling

    Returns:
        List[ContentDefinition]: Content definitions with normalized bounding boxes (0-1),
            labels, and confidence scores

    Raises:
        RuntimeError: If detection or processing fails
    """
    with torch.no_grad():
        try:
            results = docseg_model(source=path, save=False, device=DEVICE)
            return [
                {
                    "classification": "application/x-nldoc.region",
                    "bbox": {
                        "left": bbox.xyxyn[0][0].item(),
                        "top": bbox.xyxyn[0][1].item(),
                        "right": bbox.xyxyn[0][2].item(),
                        "bottom": bbox.xyxyn[0][3].item(),
                    },
                    "confidence": float(bbox.conf),
                    "attributes": {},
                    "labels": [
                        {
                            "name": REVERSE_LABELS.get(
                                docseg_model.names[int(bbox.cls)], "unknown"
                            ),
                            "confidence": float(bbox.conf),
                        }
                    ],
                    "children": [],
                }
                for result in results
                for bbox in result.boxes.cpu()
            ]
        except Exception as e:
            logger.error(f"Detection or processing failed: {str(e)}")
            raise RuntimeError("Process failed") from e
