# syntax=docker/dockerfile:1

FROM python:3.12.1-slim-bookworm as builder

WORKDIR /app

# Install build and runtime dependencies together
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    gcc \
    g++ \
    libgomp1 \
    libopenblas-dev \
    libomp-dev \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean && \
    pip install --no-cache-dir poetry==1.8.2

# Install PyTorch dependencies separately for better caching
RUN pip install --no-cache-dir torch==2.3.0 torchvision torchaudio \
    --index-url https://download.pytorch.org/whl/cpu

# Install project dependencies
COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --without dev

FROM python:3.12.1-slim-bookworm

WORKDIR /app

# Create non-root user with specific UID/GID and home directory
RUN groupadd -r -g 999 worker && \
    useradd -r -m -g worker -u 999 worker && \
    mkdir -p /home/worker/.config && \
    chown -R worker:worker /home/worker

# Set environment variables for matplotlib and ultralytics
ENV MPLCONFIGDIR=/home/worker/.config/matplotlib \
    YOLO_CONFIG_DIR=/home/worker/.config/Ultralytics

# Copy packages and code
COPY --from=builder /usr/local/lib/python3.12/site-packages/ /usr/local/lib/python3.12/site-packages/
COPY --chown=worker:worker . .

# Switch to non-root user
USER worker

CMD ["python", "-m", "main"]