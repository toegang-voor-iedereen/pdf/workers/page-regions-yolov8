import os
import sys

from kimiworker import (
    JobHandler,
    KimiLogger,
    PublishMethod,
    Worker,
    WorkerJob,
    RemoteFileStorageInterface,
)

from detect import detect
from utils import calculate_mean_confidence

numberOfConcurrentJobs = int(os.getenv("CONCURRENT_JOBS", "1"))


def get_job_handler() -> JobHandler:
    """Get the job handler"""

    def job_handler(
        log: KimiLogger,
        job: WorkerJob,
        job_id: str,
        local_file_path: str,
        remote_file_storage: RemoteFileStorageInterface,
        publish: PublishMethod,
    ):
        """Handle getting regions from page image"""

        content_result = detect(local_file_path, log)

        mean_confidence = calculate_mean_confidence(content_result)
        log.info("Job done")

        publish(
            "contentResult",
            content_result,
            success=True,
            confidence=mean_confidence,
        )

    return job_handler


if __name__ == "__main__":
    try:
        worker = Worker(get_job_handler(), "worker-page-regions-yolo", True)
        worker.start(numberOfConcurrentJobs)

    except KeyboardInterrupt:
        print("\n")
        print("Got interruption signal. Exiting...\n")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
